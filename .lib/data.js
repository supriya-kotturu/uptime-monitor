// library for storing and editing data

const fs = require("fs");
const path = require("path");
const helpers = require("./helpers");

// container for the module to export
let lib = {};

// base directory of data folder
lib.baseDir = path.join(__dirname, "/../.data/");

//  write data into a file
lib.create = (dir, file, data, callback) => {
    // open the file to write
    fs.open(`${lib.baseDir + dir}/${file}.json`, "wx", function (
        err,
        fileDescriptor
    ) {
        if (!err && fileDescriptor) {
            // convert data to string
            var stringData = JSON.stringify(data);
            // write to a file
            fs.writeFile(fileDescriptor, stringData, (err) => {
                if (!err) {
                    // close the file
                    fs.close(fileDescriptor, (err) => {
                        if (!err) {
                            callback(false);
                        } else {
                            callback("Error closing the file");
                        }
                    });
                } else {
                    callback("Error writing to a new fie");
                }
            });
        } else {
            callback("Unable to create a new file. It may already exist");
        }
    });
};

// read data from a file
lib.read = (dir, file, callback) => {
    fs.readFile(`${lib.baseDir + dir}/${file}.json`, "utf8", (err, data) => {
        if (!err && data) {
            let parsedData = helpers.parseJsonToObject(data);
            callback(err, parsedData);
        } else {
            callback(err, data);
        }
    });
};

// update data inside a file
lib.update = (dir, file, data, callback) => {
    fs.open(`${lib.baseDir + dir}/${file}.json`, "r+", (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
            let stringData = JSON.stringify(data);
            // truncate the file
            fs.ftruncate(fileDescriptor, (err) => {
                if (!err) {
                    // close the file
                    fs.writeFile(fileDescriptor, stringData, (err) => {
                        if (!err) {
                            // close the file
                            fs.close(fileDescriptor, (err) => {
                                if (!err) {
                                    callback(false);
                                } else {
                                    callback("Error closing the file");
                                }
                            });
                        } else {
                            callback("Error writing to existing fie");
                        }
                    });
                } else {
                    callback("Error Truncating the file");
                }
            });
        } else {
            callback("Couldn't open the file to update. File may not exist yet");
        }
    });
};

// delete a file
lib.delete = (dir, file, callback) => {
    fs.unlink(`${lib.baseDir + dir}/${file}.json`, (err) => {
        if (!err) {
            callback(false);
        } else {
            callback("Error deleting the file");
        }
    });
};


// List all the items in a directory
lib.list = (dir,callback)=>{
    fs.readdir(`${lib.baseDir+dir}`, (err, filesData)=>{
        if(!err && filesData && filesData.length > 0){
            let trimmedFileNames = [];
            filesData.forEach((fileName)=>{
                trimmedFileNames.push(fileName.replace('.json', ''));
            });
            callback(false, trimmedFileNames);
        }else{
            callback(err, filesData);
        }
    })
}


// export the module
module.exports = lib;