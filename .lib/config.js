/*
| This file contains all the environment variables
*/

// 10. container for all the environments
var environments = {};

// Staging environment {defaut env}
environments.staging = {
  httpPort: 3000,
  httpsPort: 3001,
  hashingSecret: "This is a magic formula",
  maxChecks: 5,
  envName: "Staging",
  twilio: {
    accountSid: "AC3233d77ecccfe0ac694147f1a52c176d",
    authToken: "321160b944be14f9a932a62ba0b6f7b9",
    fromPhone: "+12029722720",
  }
  // nexmo : {
  //     apiKey: '3174d572',
  //     apiSecret: '8pUNHN5DbIlCAzI9',
  // }
};

//  production environment
environments.production = {
  httpPort: 80,
  httpsPort: 443,
  hashingSecret: "pixie dust",
  maxChecks: 5,
  envName: "production",
  twilio: {
    accountSid: "AC3233d77ecccfe0ac694147f1a52c176d",
    authToken: "321160b944be14f9a932a62ba0b6f7b9",
    fromPhone: "+12029722720",
  }
  // nexmo : {
  //     apiKey: '3174d572',
  //     apiSecret: '8pUNHN5DbIlCAzI9',
  // }
};

// exporting the environment object based in the arguments passed on the CLI

let currentEnvironment =
  typeof process.env.NODE_ENV == "string"
    ? process.env.NODE_ENV.toLowerCase()
    : "";

// check the current environment is one of the environments above. if not, default to staging

let environmentToExport =
  typeof environments[currentEnvironment] == "object"
    ? environments[currentEnvironment]
    : environments.staging;

module.exports = environmentToExport;

// for Windows
// SET NODE_ENV=production
// node index.js
