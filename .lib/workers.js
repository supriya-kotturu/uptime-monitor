// These are worker related tasks

// DEPENDENCIES
const path = require("path");
const fs = require("fs");
const url = require("url");
const http = require("http");
const https = require("https");
const _data = require("./data");
const helpers = require("./handlers");
const _logs = require("./logs");
const util = require("util");
const debug = util.debuglog("workers");

//  Instantiate the Worker object
const workers = {};

// Loop all the checks, get their data, send it to a validator
workers.gatherAllChecks = () => {
  // get all the checks that exist in the system
  _data.list("checks", (err, checks) => {
    if (!err && checks && checks.length > 0) {
      checks.forEach((check) => {
        // Read the check data
        _data.read("checks", check, (err, originalCheckData) => {
          if (!err && originalCheckData) {
            // pass the data to the check validator, and let that function continue or log the errors encountered
            workers.validateCheckData(originalCheckData);
          } else {
            debug(`Error : Error reading one of the checks to process`);
          }
        });
      });
    } else {
      debug(`Error : Could not find any checks to process`);
    }
  });
};

// Sanity checking the check data
workers.validateCheckData = (originalCheckData) => {
  originalCheckData =
    typeof originalCheckData === "object" && originalCheckData != null
      ? originalCheckData
      : {};

  originalCheckData.id =
    typeof originalCheckData.id === "string" &&
    originalCheckData.id.length == 20
      ? originalCheckData.id
      : false;

  originalCheckData.userPhone =
    typeof originalCheckData.userPhone === "string" &&
    originalCheckData.userPhone.length == 10
      ? originalCheckData.userPhone
      : false;

  originalCheckData.protocol =
    typeof originalCheckData.protocol.trim() === "string" &&
    ["http", "https"].indexOf(originalCheckData.protocol.trim()) > -1
      ? originalCheckData.protocol.trim()
      : false;

  originalCheckData.url =
    typeof originalCheckData.url.trim() === "string" &&
    originalCheckData.url.trim().length > 0
      ? originalCheckData.url.trim()
      : false;

  originalCheckData.method =
    typeof originalCheckData.method.trim() === "string" &&
    ["get", "post", "put", "delete"].indexOf(
      originalCheckData.method.trim().toLowerCase()
    ) > -1
      ? originalCheckData.method.trim().toLowerCase()
      : false;

  originalCheckData.successCodes =
    typeof originalCheckData.successCodes === "object" &&
    originalCheckData.successCodes instanceof Array &&
    originalCheckData.successCodes.length > 0
      ? originalCheckData.successCodes
      : false;

  originalCheckData.timeoutSeconds =
    typeof originalCheckData.timeoutSeconds === "number" &&
    originalCheckData.timeoutSeconds % 1 === 0 &&
    originalCheckData.timeoutSeconds >= 1 &&
    originalCheckData.timeoutSeconds <= 5
      ? originalCheckData.timeoutSeconds
      : false;

  // Set the keys that may not be set if the workers have never seen the check
  originalCheckData.state =
    typeof originalCheckData.state === "string" &&
    ["up", "down"].indexOf(originalCheckData.state) > -1
      ? originalCheckData.state
      : "down";

  originalCheckData.lastChecked =
    typeof originalCheckData.lastChecked === "number" &&
    originalCheckData.timeoutSeconds % 1 === 0 &&
    originalCheckData.lastChecked >= 1 &&
    originalCheckData.lastChecked <= 5
      ? originalCheckData.lastChecked
      : false;

  // If all the checks pass, pass the data to the next step the process
  if (
    originalCheckData.id &&
    originalCheckData.userPhone &&
    originalCheckData.protocol &&
    originalCheckData.url &&
    originalCheckData.method &&
    originalCheckData.successCodes &&
    originalCheckData.timeoutSeconds
  ) {
    workers.performCheck(originalCheckData);
  } else {
    debug(
      `Error : One of the checks(${originalCheckData.id}) is not properly formatted. Skipping it.`
    );
  }
};

// Perform the check, send the original checkdata and the outcome of check process, to the next step in the process
workers.performCheck = (originalCheckData) => {
  // Prepare the initial check outcome
  let checkOutcome = {};
  checkOutcome = {
    error: false,
    responseCode: false,
  };

  // Mark that the outcome has not been sent yet
  let outcomeSent = false;

  // Parse the hostname and the path out of the original check data
  let parsedURL = url.parse(
    `${originalCheckData.protocol}://${originalCheckData.url}`,
    true
  );
  let hostName = parsedURL.hostName;
  let path = parsedURL.path; // Using path and not "pathname" because we waant the query string

  // Constructing the request
  let requestDetails = {
    protocol: `${originalCheckData.protocol}:`,
    hostname: hostName,
    method: originalCheckData.method.toUpperCase(),
    path: path,
    timeout: originalCheckData.timeoutSeconds * 1000,
  };

  // Instantiate the request object (using either HTTP or HTTPS module)
  let _moduleToUse = originalCheckData.protocol == "http" ? http : https;

  let req = _moduleToUse.request(requestDetails, (res) => {
    // Grab the status of the sent request
    let status = res.statusCode;

    // Update the checkOtcome and pass the data along
    checkOutcome.responseCode = status;
    if (!outcomeSent) {
      workers.processCheckOutcome(originalCheckData.checkOutcome);
      outcomeSent = true;
    }
  });

  // Bind to the error event so it doesnt get thrown
  req.on("error", (err) => {
    // Update the checkOtcome and pass the data along
    checkOutcome.error = {
      error: true,
      value: err,
    };
    if (!outcomeSent) {
      workers.processCheckOutcome(originalCheckData, checkOutcome);
      outcomeSent = true;
    }
  });

  // Bind to the timeout
  req.on("timeout", (err) => {
    // Update the checkOtcome and pass the data along
    checkOutcome.error = {
      error: true,
      value: "timeout",
    };
    if (!outcomeSent) {
      workers.processCheckOutcome(originalCheckData, checkOutcome);
      outcomeSent = true;
    }
  });

  //  End the request
  req.end();
};

// Process the checkOutcome, update the check data and trigger an alert if needed
// Special logic for accomodating a check that has never been tested before (don't alert on that one)

// @TODO refactor it using Express
workers.processCheckOutcome = (originalCheckData, checkOutcome) => {
  // decide if the check is considered up or down
  let state =
    !checkOutcome.error &&
    checkOutcome.responseCode &&
    checkOutcome.successCodes.indexOf(checkOutcome.responseCode > -1)
      ? "up"
      : "down";

  // Decide if an alert is wanted
  let alertWanted =
    originalCheckData.lastChecked && originalCheckData.state !== state
      ? true
      : false;

  // update the check data
  let newCheckData = originalCheckData;
  newCheckData.state = state;
  newCheckData.lastChecked = Date.now();

  // log the outcome
  let timeOfCheck = originalCheckData.lastChecked;
  workers.log(originalCheckData, checkOutcome, state, alertWanted, timeOfCheck);

  // Save the updates
  _data.update("checks", newCheckData.id, newCheckData, (err) => {
    if (!err) {
      // Send the new check data to the next step of the process
      if (alertWanted) {
        workers.alertTheUserToStatusChange(newCheckData);
      } else {
        debug("Check outcome has not changed, no alert wanted");
      }
    } else {
      debug(
        "Error occured while trying to save the updates to one of the checks"
      );
    }
  });
};

// Alert the user as to change in their check status
workers.alertTheUserToStatusChange = (newCheckData) => {
  let msg = `Alert : Your Check for ${newCheckData.method.toUpperCase()} ${
    newCheckData.protocol
  }://${newCheckData.url} is currently ${newCheckData.state}`;

  helpers.sendTwillioSms(newCheckData.userPhone, msg, (err) => {
    if (!err) {
      debug(
        `Success : User was alerted to a status change in their check, via SMS : ${msg}`
      );
    } else {
      debug(
        `Error : Could not send SMS to the user who had a state change in their check`
      );
    }
  });
};

//  Log the data into the file
workers.log = (
  originalCheckData,
  checkOutcome,
  state,
  alertWanted,
  timeOfCheck
) => {
  // Form the log data
  let logData = {
    check: originalCheckData,
    outcome: checkOutcome,
    state: state,
    alertWanted: alertWanted,
    time: timeOfCheck,
  };

  // Convert data into a String
  let logString = JSON.stringify(logData);

  // Determine the name of the log file
  let logFileName = originalCheckData.id;

  // Append the log string to the file
  _logs.append(logFileName, logString, (err) => {
    if (!err) {
      debug("Logging to file Succeeded");
    } else {
      debug("Failed to log the data into the file");
    }
  });
};

// Timer to execute the worker-process once per minute
workers.loop = () => {
  setInterval(() => {
    workers.gatherAllChecks();
  }, 1000 * 60);
};

// Rotate (compresS) the log file
workers.rotatelogs = () => {
  // list all the (non compressed) log files
  _logs.list(false, (err, logs) => {
    if (!err && logs && logs.length > 0) {
      logs.forEach((logName) => {
        // compress the data to a different file
        let logId = logName.replace(".log", "");
        let newFileId = logId + "-" + Date.now();

        _logs.compress(logId, newFileId, (err) => {
          if (!err) {
            // Truncate the log
            _logs.truncate(logId, (err) => {
              if (!err) {
                debug("Success truncating the log file");
              } else {
                debug("Error truncating the log file", err);
              }
            });
          } else {
            debug("Error Compressing one of the log files", err);
          }
        });
      });
    } else {
      debug("Error : Could not find any logs to rotate");
    }
  });
};

// Timer to exectue the log rotation process once per day
workers.logRotationLoop = () => {
  setInterval(() => {
    workers.rotatelogs();
  }, 1000 * 60 * 60 * 24);
};

// Init script
workers.init = () => {
  // Send to the console in yellow
  console.log("\x1b[33m%s\x1b[0m", "Background workers are running");

  // Execute all the checks
  workers.gatherAllChecks();

  // Call the loop so that the checks wil continue later on
  workers.loop();

  // Compress all the logs immediately
  workers.rotatelogs();

  //  Call the compression loop to compress the logs later on
  workers.logRotationLoop();
};

// Export the worker
module.exports = workers;
