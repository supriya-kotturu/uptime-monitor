// helpers for various tasks

// DEPENDENCIES
const crypto = require("crypto");
const config = require("./config");
const https = require("https");
const queryString = require("querystring");
const twilio = require("twilio");

// container for all the helpers
let helpers = {};

// Create SHA256 hash
helpers.hash = (str) => {
  if (typeof str === "string" && str.trim().length > 0) {
    let hash = crypto
      .createHmac("sha256", config.hashingSecret)
      .update(str)
      .digest("hex");
    return hash;
  } else {
    return false;
  }
};

// parse JSON string to Object in all casses without throwing
helpers.parseJsonToObject = (str) => {
  try {
    let obj = JSON.parse(str);
    return obj;
  } catch (e) {
    return {};
  }
};

// create a random string of alpha numeric characters of a given lenght
helpers.createRandomString = (strLen) => {
  strLen = typeof strLen == "number" && strLen > 0 ? strLen : false;
  if (strLen) {
    // list all the characters that can go into the string
    let possibleCharacters = "abcdefghijklmnopqrstuvwxyz0987654321";
    //  start the final string
    let str = "";
    for (let i = 0; i < strLen; i++) {
      // get random characters from the possible characters
      let randomCharacter = possibleCharacters.charAt(
        Math.floor(Math.random() * possibleCharacters.length)
      );
      // append the character to the final str
      str += randomCharacter;
    }
    // Return the final string
    return str;
  } else {
    return false;
  }
};

// Send SMS messages via Twilio
helpers.sendTwilioSms = (phone, msg, callback) => {
  // validate the parameters
  phone =
    typeof phone === "string" && phone.trim().length == 10
      ? phone.trim()
      : false;
  msg =
    typeof msg === "string" && msg.trim().length <= 1600 ? msg.trim() : false;

  if (phone && msg) {
    // using 3rd party library
    const client = new twilio(
      config.twilio.accountSid,
      config.twilio.authToken
    );
    client.messages
      .create({
        body: msg,
        to: "+91" + phone,
        from: config.twilio.fromPhone,
      })
      .then((message) => console.log(`message sent : ${message.body}`))
      .catch((err) => callback(err));

    // // configure the request payload
    // let payload = {
    //   From: config.twilio.fromPhone,
    //   To: `+91${phone}`,
    //   Body: msg,
    // };

    // // stringify the payload
    // let stringPayload = queryString.stringify(payload);

    // // Configure the request utils
    // // refered how twilio works in curl command
    // let requestDetails = {
    //   protocol: "https:",
    //   hostname: "api.twilio.com",
    //   method: "POST",
    //   path: `/2010-04-01/Accounts/${config.twilio.accountSid}/Messages.json`,
    //   auth: `${config.twilio.accountSid} : ${config.twilio.authToken}`,
    //   headers: {
    //     "Content-Type": "application/x-www-form-url-encoded",
    //     "Content-Length": Buffer.byteLength(stringPayload),
    //   },
    // };

    // // instantiate the request object
    // let req = https.request(requestDetails, (res) => {
    //   // Grab the status of the sent request
    //   let status = res.status;

    //   // callback successfully if the request went through
    //   if (status == 200 || status == 201) {
    //     callback(false);
    //   } else {
    //     callback("Status code returned was : ", status);
    //   }
    // });

    // // bind to the error event so that it doesn't get thrown
    // req.on("error", () => {
    //   callback(e);
    // });

    // // add the payload
    // req.write(stringPayload);

    // // end the request
    // req.end();
  } else {
    callback("Given parameters were missing or invalid");
  }
};

// Export the container
module.exports = helpers;
