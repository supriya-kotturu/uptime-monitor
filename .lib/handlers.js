// library for REQUEST HANDLERS

// DEPENDENCIES
const _data = require("./data.js");
const helpers = require("./helpers");
const config = require("./config");
const { type } = require("os");

// 8. define Request handlers module to export
let handlers = {};

// get the method from the data object and pass it to the respective sub-handlers
handlers.users = (reqData, callback) => {
  let acceptableMethods = ["POST", "GET", "PUT", "DELETE"];
  if (acceptableMethods.indexOf(reqData.method) > -1) {
    handlers._users[reqData.method.toLowerCase()](reqData, callback);
  } else {
    callback(405);
  }
};

// container for USERS sub-methods
handlers._users = {};

// USERS - POST
// Required data : firstName, lastName, phone, password, tosAgreement
// Optional data : none
handlers._users.post = (reqData, callback) => {
  // check if all the required fields are filled out
  let firstName =
    typeof reqData.payload.firstName === "string" &&
    reqData.payload.firstName.trim().length > 0
      ? reqData.payload.firstName.trim()
      : false;
  let lastName =
    typeof reqData.payload.lastName === "string" &&
    reqData.payload.lastName.trim().length > 0
      ? reqData.payload.lastName.trim()
      : false;
  let phone =
    typeof reqData.payload.phone === "string" &&
    reqData.payload.phone.trim().length === 10
      ? reqData.payload.phone.trim()
      : false;
  let password =
    typeof reqData.payload.password === "string" &&
    reqData.payload.password.trim().length > 0
      ? reqData.payload.password.trim()
      : false;
  let tosAgreement =
    typeof reqData.payload.tosAgreement === "boolean" &&
    reqData.payload.tosAgreement === true
      ? true
      : false;

  if (firstName && lastName && phone && password && tosAgreement) {
    // make sure that the user doesnt already exist
    _data.read("users", phone, (err, data) => {
      if (err) {
        // Hash the password
        let hashedPassword = helpers.hash(password);
        if (hashedPassword) {
          // create USER object
          let userObject = {
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            hashedPassword: hashedPassword,
            tosAgreement: tosAgreement,
          };

          // store the user
          _data.create("users", phone, userObject, (err) => {
            if (!err) {
              callback(200);
            } else {
              console.log(err);
              callback(500, {
                Error: "Could not create the new user",
              });
            }
          });
        } else {
          console.log(err);
          callback(500, {
            Error: "Coudld not hash the user's password",
          });
        }
      } else {
        callback(400, {
          Error: `A user already exists with phone number : ${phone}`,
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing or Incorrect Required fields",
    });
  }
};

// USERS - GET
// Required data : phone
// Optional data : none
handlers._users.get = (reqData, callback) => {
  // check the phone number provided is valid
  // since it is a GET request, there's no payload.
  // we are getting the phone from the QUERYSTRINGOBJECT

  let phone =
    typeof reqData.queryStringObject.phone === "string" &&
    reqData.queryStringObject.phone.trim().length === 10
      ? reqData.queryStringObject.phone.trim()
      : false;
  if (phone) {
    // get the token from the headers
    let token =
      typeof reqData.headers.token === "string" ? reqData.headers.token : false;

    // verify that the given token is valid for the phone number
    handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
      if (tokenIsValid) {
        // lookup the user by reading the token
        _data.read("users", phone, (err, userData) => {
          if (!err && userData) {
            // remove the hashed password from the user object before returning it to the user
            // userData: refers to the file data which is sent after being read
            // _data: refers to the internal module to perform actions on file
            delete userData.hashedPassword;
            callback(200, userData);
          } else {
            console.log(err);
            callback(404, {
              Error: "User Not Found",
            });
          }
        });
      } else {
        callback(403, {
          Error: "Missing token in header or the token is invalid",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required required field",
    });
  }
};

// USERS - PUT
// Required Data : phone
// Optional Data : firstName, lastName, password (atleast one must be specified)
handlers._users.put = (reqData, callback) => {
  // check for the required field
  let phone =
    typeof reqData.payload.phone === "string" &&
    reqData.payload.phone.trim().length === 10
      ? reqData.payload.phone.trim()
      : false;

  // check for the optional fields
  let firstName =
    typeof reqData.payload.firstName === "string" &&
    reqData.payload.firstName.trim().length > 0
      ? reqData.payload.firstName.trim()
      : false;
  let lastName =
    typeof reqData.payload.lastName === "string" &&
    reqData.payload.lastName.trim().length > 0
      ? reqData.payload.lastName.trim()
      : false;
  let password =
    typeof reqData.payload.password === "string" &&
    reqData.payload.password.trim().length > 0
      ? reqData.payload.password.trim()
      : false;

  // Error if the phone is invalid
  if (phone) {
    let token =
      typeof reqData.headers.token === "string" ? reqData.headers.token : false;

    // verify that the given token is valid for the phone number
    handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
      if (tokenIsValid) {
        // Error if there's nothing to update
        if (firstName || lastName || password) {
          // lookup the user by reading the token
          _data.read("users", phone, (err, userData) => {
            if ((!err, userData)) {
              if (firstName) {
                userData.firstName = firstName;
              }
              if (lastName) {
                userData.lastName = lastName;
              }
              if (password) {
                userData.hashedPassword = helpers.hash(password);
              }

              //  store the new updates
              _data.update("users", phone, userData, (err) => {
                if (!err) {
                  callback(200);
                } else {
                  console.log(err);
                  callback(500, {
                    Error: "Couldn't update the user",
                  });
                }
              });
            } else {
              console.log(err);
              callback(400, {
                Error: "The specified user does not Exist",
              });
              //  not preferable to use a 404 in a PUT request
            }
          });
        } else {
          callback(403, {
            Error: "Missing fields to update",
          });
        }
      } else {
        callback(403, {
          Error: "Missing required token in header, or token is invalid",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required field",
    });
  }
};

// USERS - DELETE
// Required fields : phone
// Optional parameter : none
handlers._users.delete = (reqData, callback) => {
  let phone =
    typeof reqData.queryStringObject.phone === "string" &&
    reqData.queryStringObject.phone.trim().length === 10
      ? reqData.queryStringObject.phone.trim()
      : false;
  if (phone) {
    // lookup the user
    let token =
      typeof reqData.headers.token === "string" ? reqData.headers.token : false;

    // verify that the given token is valid for the phone number
    handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
      if (tokenIsValid) {
        _data.read("users", phone, (err, userData) => {
          if (!err && userData) {
            _data.delete("users", phone, (err) => {
              if (!err) {
                // delete each of the checks associated with the user
                let userChecks =
                  typeof userData.checks === "object" &&
                  userData.checks instanceof Array
                    ? userData.checks
                    : [];
                let checksToDelete = userChecks.length;

                if (checksToDelete > 0) {
                  let checksDeleted = 0;
                  let deletionErrors = false;

                  // loop through the checks to delete all the checks
                  userChecks.forEach((checkId) => {
                    // delete the check
                    _data.delete("checks", checkId, (err) => {
                      if (err) {
                        deletionErrors = true;
                      }
                      checksDeleted++;
                      if (checksDeleted === checksToDelete) {
                        if (!deletionErrors) {
                          callback(200);
                        } else {
                          callback(500, {
                            Error:
                              "Error encountered while attempting to delete all of the user's checks. All checks may not have been deleted from the system successfuly",
                          });
                        }
                      }
                    });
                  });
                } else {
                  callback(200);
                }
              } else {
                callback(500, {
                  Error: "Could not delete the specified user",
                });
              }
            });
          } else {
            console.log(err);
            callback(400, {
              Error: "Could not find the specified user",
            });
          }
        });
      } else {
        callback(403, {
          Error: "Missing required token in header, or token is",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required field",
    });
  }
};

handlers.tokens = (reqData, callback) => {
  let acceptableMethods = ["POST", "GET", "PUT", "DELETE"];
  if (acceptableMethods.indexOf(reqData.method) > -1) {
    handlers._tokens[reqData.method.toLowerCase()](reqData, callback);
  } else {
    callback(405);
  }
};

// container for all the tokens methods
handlers._tokens = {};

// TOKENS - POST
// Required data : phone, password
// Optional data : none
handlers._tokens.post = (reqData, callback) => {
  let phone =
    typeof reqData.payload.phone === "string" &&
    reqData.payload.phone.trim().length === 10
      ? reqData.payload.phone.trim()
      : false;
  let password =
    typeof reqData.payload.password === "string" &&
    reqData.payload.password.trim().length > 0
      ? reqData.payload.password.trim()
      : false;
  if (phone && password) {
    //  lookup the user who matches that phone number
    _data.read("users", phone, (err, userData) => {
      if (!err && userData) {
        // lookup the user who matches that phone number
        _data.read("users", phone, (err, userData) => {
          if (!err && userData) {
            // hash the sent password and compare it to the password stored in the user object
            let hashedPassword = helpers.hash(password);
            if (hashedPassword == userData.hashedPassword) {
              // if valid, create a token with a valid name and
              // Set expiration date 1 hour in the future.
              let tokenId = helpers.createRandomString(20);
              let expires = Date.now() + 1000 * 60 * 60;
              let tokenObject = {
                phone: phone,
                id: tokenId,
                expires: expires,
              };

              // store the token
              _data.create("tokens", tokenId, tokenObject, (err) => {
                if (!err) {
                  callback(200, tokenObject);
                } else {
                  callback(500, {
                    Error: "Could not create a new token",
                  });
                }
              });
            } else {
              callback(400, {
                Error:
                  "Password did not match the specified user's stored password",
              });
            }
          } else {
            callback(400, {
              Error: "Could not find the specified user",
            });
          }
        });
      } else {
        callback(400, {
          Error: "Could not find a specified user",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required fields",
    });
  }
};

// TOKENS - GET
// Required data : token ID
// Optional dat : none
handlers._tokens.get = (reqData, callback) => {
  // check the Id (token ID) provided is valid
  // since it is a GET request, there's no payload.
  // we are getting the phone from the QUERYSTRINGOBJECT

  let id =
    typeof reqData.queryStringObject.id === "string" &&
    reqData.queryStringObject.id.trim().length === 20
      ? reqData.queryStringObject.id.trim()
      : false;
  if (id) {
    // lookup the token by reading the token
    _data.read("tokens", id, (err, tokenData) => {
      if (!err && tokenData) {
        // tokenData: refers to the file data which is sent after being read
        // _data: refers to the internal module to perform actions on file
        callback(200, tokenData);
      } else {
        console.log(err);
        callback(404);
      }
    });
  } else {
    console.log("token legnth is not 20");
    callback(400, {
      Error: "Missing required field",
    });
  }
};

// TOKENS - PUT
// Required data : id
// Optiona data : none
handlers._tokens.put = (reqData, callback) => {
  let id =
    typeof reqData.payload.id === "string" &&
    reqData.payload.id.trim().length == 20
      ? reqData.payload.id.trim()
      : false;
  let extend =
    typeof reqData.payload.extend === "boolean" &&
    reqData.payload.extend == true
      ? reqData.payload.extend
      : false;
  if (id && extend) {
    // lookup the token by reading the token
    _data.read("tokens", id, (err, tokenData) => {
      if (!err && tokenData) {
        // check to make sure if the token isnt already expired
        if (tokenData.expires > Date.now()) {
          // set the expiration an hour from now
          tokenData.expires = Date.now() + 1000 * 60 * 60;

          // store the new updates
          _data.update("tokens", id, tokenData, (err) => {
            if (!err) {
              callback(200);
            } else {
              callback(500, {
                Error: "Could not update the token's expiration",
              });
            }
          });
        } else {
          callback(400, {
            Error: "Token has been expired and cannot be extended",
          });
        }
      } else {
        callback(400, {
          Error: "Specified token doesn't exist",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required field(s) or Field(s) are invalid",
    });
  }
};

// TOKENS - DELETE
// Required data : Id
// Optional data : none
handlers._tokens.delete = (reqData, callback) => {
  let id =
    typeof reqData.queryStringObject.id === "string" &&
    reqData.queryStringObject.id.trim().length === 20
      ? reqData.queryStringObject.id.trim()
      : false;
  if (id) {
    // lookup the token by reading the token
    _data.read("tokens", id, (err, tokenData) => {
      if (!err && tokenData) {
        _data.delete("tokens", id, (err) => {
          if (!err) {
            callback(200);
          } else {
            callback(500, {
              Error: "Could not delete the specified token",
            });
          }
        });
      } else {
        console.log(err);
        callback(400, {
          Error: "Could not find the specified token",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required field",
    });
  }
};

// Verify the given token Id is currently valid for a given user
handlers._tokens.verifyToken = (id, phone, callback) => {
  // look up the token
  _data.read("tokens", id, (err, tokenData) => {
    if (!err && tokenData) {
      // check that the token is from the given user has not expired
      if (tokenData.phone == phone && tokenData.expires > Date.now()) {
        callback(true);
      } else {
        callback(false);
      }
    } else {
      callback(false);
    }
  });
};

handlers.checks = (reqData, callback) => {
  let acceptableMethods = ["POST", "GET", "PUT", "DELETE"];
  if (acceptableMethods.indexOf(reqData.method) > -1) {
    handlers._checks[reqData.method.toLowerCase()](reqData, callback);
  } else {
    callback(405);
  }
};

handlers._checks = {};

// CHECKS - POST
// Required data : protocol [http/ https], url, method, successCodes, timeoutSeconds
// Optional data : none
handlers._checks.post = (reqData, callback) => {
  let protocol =
    typeof reqData.payload.protocol.trim() === "string" &&
    ["http", "https"].indexOf(reqData.payload.protocol.trim()) > -1
      ? reqData.payload.protocol.trim()
      : false;
  let url =
    typeof reqData.payload.url.trim() === "string" &&
    reqData.payload.url.trim().length > 0
      ? reqData.payload.url.trim()
      : false;
  let method =
    typeof reqData.payload.method.trim() === "string" &&
    ["get", "post", "put", "delete"].indexOf(
      reqData.payload.method.trim().toLowerCase()
    ) > -1
      ? reqData.payload.method.trim().toLowerCase()
      : false;
  let successCodes =
    typeof reqData.payload.successCodes === "object" &&
    reqData.payload.successCodes instanceof Array &&
    reqData.payload.successCodes.length > 0
      ? reqData.payload.successCodes
      : false;
  let timeoutSeconds =
    typeof reqData.payload.timeoutSeconds === "number" &&
    reqData.payload.timeoutSeconds % 1 === 0 &&
    reqData.payload.timeoutSeconds >= 1 &&
    reqData.payload.timeoutSeconds <= 5
      ? reqData.payload.timeoutSeconds
      : false;

  if (protocol && url && method && successCodes && timeoutSeconds) {
    // get the token from the headers
    let token =
      typeof reqData.headers.token == "string" ? reqData.headers.token : false;

    // look up the user by reading the token
    _data.read("tokens", token, (err, tokenData) => {
      if (!err && tokenData) {
        // get the user phone from the token Data
        let userPhone = tokenData.phone;

        // lookup the user data
        _data.read("users", userPhone, (err, userData) => {
          if (!err && userData) {
            let userChecks =
              typeof userData.checks === "object" &&
              userData.checks instanceof Array
                ? userData.checks
                : [];

            //  verify that the user has less than the number of maxChecks per user
            if (userChecks.length < config.maxChecks) {
              // create a Random ID for the check
              let checkId = helpers.createRandomString(20);

              // create the check object, and include the user's phone
              let checkObject = {
                id: checkId,
                userPhone: userPhone,
                protocol: protocol,
                url: url,
                method: method,
                successCodes: successCodes,
                timeoutSeconds: timeoutSeconds,
              };

              // save the check object
              _data.create("checks", checkId, checkObject, (err) => {
                if (!err) {
                  // add the check Id to the user's object
                  userData.checks = userChecks;
                  userData.checks.push(checkId);

                  // save the new user data
                  _data.update("users", userPhone, userData, (err) => {
                    if (!err) {
                      callback(200, checkObject);
                    } else {
                      callback(500, {
                        Error: "Could not update the user with the new check",
                      });
                    }
                  });
                } else {
                  callback(500, { Error: `Could not create the new check` });
                }
              });
            } else {
              callback(400, {
                Error: `The User already has maximum number of checks (${config.maxChecks})`,
              });
            }
          } else {
            console.log("No user found with given token");
            callback(403, {
              Error: "Unauthorized Request",
            });
          }
        });
      } else {
        callback(403, {
          Error: "Unauthorized Request",
        });
      }
    });
  } else {
    callback(400, {
      Error: "Missing required inputs, or inputs are invalid",
    });
  }
};

// Checks - GET
// Required data : id
// Optional data : none
handlers._checks.get = (reqData, callback) => {
  // check the checkId provided is valid
  // since it is a GET request, there's no payload.
  // we are getting the phone from the QUERYSTRINGOBJECT

  let id =
    typeof reqData.queryStringObject.id === "string" &&
    reqData.queryStringObject.id.trim().length === 20
      ? reqData.queryStringObject.id.trim()
      : false;
  if (id) {
    // lookup the check
    _data.read("checks", id, (err, checkData) => {
      if (!err && checkData) {
        // get the token from the headers
        let token =
          typeof reqData.headers.token === "string"
            ? reqData.headers.token
            : false;

        // verify that the given token is valid and belongs to the user who created the check
        handlers._tokens.verifyToken(
          token,
          checkData.userPhone,
          (tokenIsValid) => {
            if (tokenIsValid) {
              // lookup the user by reading the token
              _data.read("users", checkData.userPhone, (err, userData) => {
                if (!err && userData) {
                  //  return the check data
                  callback(200, checkData);
                } else {
                  console.log(err);
                  callback(403);
                }
              });
            } else {
              callback(403, {
                Error: "Missing token in header or the token is invalid",
              });
            }
          }
        );
      } else {
        callback(404);
      }
    });
  } else {
    callback(400, {
      Error: "Missing required required field",
    });
  }
};

// Checks - PUT
// Required data : Id
// optional data : protocol, url, method, successCodes, timeoutSeconds (one must be sent)
handlers._checks.put = (reqData, callback) => {
  // check for the required field
  let id =
    typeof reqData.payload.id === "string" &&
    reqData.payload.id.trim().length === 20
      ? reqData.payload.id.trim()
      : false;

  // check for the optional fields
  let protocol =
    typeof reqData.payload.protocol.trim() === "string" &&
    ["http", "https"].indexOf(reqData.payload.protocol.trim()) > -1
      ? reqData.payload.protocol.trim()
      : false;
  let url =
    typeof reqData.payload.url.trim() === "string" &&
    reqData.payload.url.trim().length > 0
      ? reqData.payload.url.trim()
      : false;
  let method =
    typeof reqData.payload.method.trim() === "string" &&
    ["get", "post", "put", "delete"].indexOf(
      reqData.payload.method.trim().toLowerCase()
    ) > -1
      ? reqData.payload.method.trim().toLowerCase()
      : false;
  let successCodes =
    typeof reqData.payload.successCodes === "object" &&
    reqData.payload.successCodes instanceof Array &&
    reqData.payload.successCodes.length > 0
      ? reqData.payload.successCodes
      : false;
  let timeoutSeconds =
    typeof reqData.payload.timeoutSeconds === "number" &&
    reqData.payload.timeoutSeconds % 1 === 0 &&
    reqData.payload.timeoutSeconds >= 1 &&
    reqData.payload.timeoutSeconds <= 5
      ? reqData.payload.timeoutSeconds
      : false;

  // check if check Id is valid
  if (id) {
    // check to make sure one or more optional fields has been sent
    if (protocol || url || method || successCodes || timeoutSeconds) {
      // lookup the check
      _data.read("checks", id, (err, checkData) => {
        if (!err && checkData) {
          // get the token from the headers
          let token =
            typeof reqData.headers.token === "string"
              ? reqData.headers.token
              : false;

          // verify that the given token is valid and belongs to the user who created the check
          handlers._tokens.verifyToken(
            token,
            checkData.userPhone,
            (tokenIsValid) => {
              if (tokenIsValid) {
                // update the checkData where necessary
                if (protocol) {
                  checkData.protocol = protocol;
                }
                if (url) {
                  checkData.url = url;
                }
                if (method) {
                  checkData.method = method;
                }
                if (successCodes) {
                  checkData.successCodes = successCodes;
                }
                if (timeoutSeconds) {
                  checkData.timeoutSeconds = timeoutSeconds;
                }

                // store the new updates
                _data.update("checks", id, checkData, (err) => {
                  if (!err) {
                    callback(200);
                  } else {
                    callback(500, { Error: "Could not update the check" });
                  }
                });
              } else {
                callback(403);
              }
            }
          );
        } else {
          callback(400, { Error: "Check ID does not exist" });
        }
      });
    } else {
      callback(400, { Error: "Missing fields to update" });
    }
  } else {
    callback(400, {
      Error: "Missing required field",
    });
  }
};

// Checks - DELETE
// Required data : CheckId
// Optional data : none
handlers._checks.delete = (reqData, callback) => {
  let id =
    typeof reqData.queryStringObject.id === "string" &&
    reqData.queryStringObject.id.trim().length === 20
      ? reqData.queryStringObject.id.trim()
      : false;

  if (id) {
    // check if the Check ID exists
    _data.read("checks", id, (err, checkData) => {
      if (!err && checkData) {
        // get the token ID from the headers
        let token =
          typeof reqData.headers.token === "string" &&
          reqData.headers.token.length === 20
            ? reqData.headers.token
            : false;

        // verify thats the given token is valid for the phone number
        handlers._tokens.verifyToken(
          token,
          checkData.userPhone,
          (tokenIsValid) => {
            if (tokenIsValid) {
              // Delete the check data
              _data.delete("checks", id, (err) => {
                if (!err) {
                  // look up the user
                  _data.read("users", checkData.userPhone, (err, userData) => {
                    if (!err && userData) {
                      //  get the checks from the user data
                      let userChecks =
                        typeof userData.checks === "object" &&
                        userData.checks instanceof Array
                          ? userData.checks
                          : [];

                      // remove the deleted check ID from the list of checks
                      console.log(userChecks);
                      let checkPosition = userChecks.indexOf(id);
                      if (checkPosition > -1) {
                        userChecks.splice(checkPosition, 1);

                        // update the user data
                        userData.checks = userChecks;

                        // re-save the user's data
                        _data.update(
                          "users",
                          checkData.userPhone,
                          userData,
                          (err) => {
                            if (!err) {
                              callback(200);
                            } else {
                              callback(500, {
                                Error: "Could not update the user data",
                              });
                            }
                          }
                        );
                      } else {
                        callback(500, {
                          Error:
                            "Could not find the check on ther user's object, so could not remove it",
                        });
                      }
                    } else {
                      callback(500, {
                        Error:
                          "Could not find the user who created the check, so cannot remove the check from the list of checks in the user Object",
                      });
                    }
                  });
                } else {
                  callback(500, { Error: "Could not delete the check data" });
                }
              });
            } else {
              callback(403);
            }
          }
        );
      } else {
        callback(400, { Error: "The specified check ID does not exist" });
      }
    });
  } else {
    callback(400, { Error: "Missing required fields" });
  }
};

handlers.ping = (data, callback) => {
  // callback a HTTPS status code and a payload object
  callback(200);
};

handlers.notFound = (data, callback) => {
  callback(404);
};

// export the module
module.exports = handlers;
