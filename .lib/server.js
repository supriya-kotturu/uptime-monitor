/*
|   Always test using POSTMAN[Recommended] or 
|   Browser instead of curl as you progress
*/

/* ------------------ DESCRIPTION ------------------ 
| 1. define a http module to describe what the server does.
| 2. we need to tell the server to listen on the particular port.
| 3. listen on the port and figure out resources are they requesting 
|       when they send an API and PARSE the url that they are asking for.
| 4. get HTTP Method [GET/POST/PUT/DELETE/HEAD] the user is requesting from the req.
| 5. get the QueryString as an Object
|       all the queries passed into the URL are parsed and put into an object 
|       with key-value pairs. Eg: {sendUserData : true , limit: 34}
| 6. get the headers as an Object which may containg authorization tokens or private keys
| 7. get the payload if there's any
| 8. Set up a request handler
| 9. Set up a router to route the requests to some request handlers
| 10. create a config.js for environment variables 
| 11. add HTTPS support by creating certificate
|   - download openSSL and set OPENSSL_CONF and path variable
|   - openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem
|   - http is going to happen in port:80
|   - https is going to happen in port:443
|   - now we listen on 2 ports for http and https
| 12. create a single function which takes req and res for createServer() function for both http and https
|   - create a unifiedServer() which gets called on http and https
*/

/* ------------------ DEPENDENCIES ------------------ 
| http :   this is the HTTP server which lets you
|          listen on ports and respond with data
| https : similar to HTTP, but requires a key and certificate
| url :    to parse the url that is being requested
| string_decoder : we use StringDecoder property obejct from this module
| config : private module where the environment variables are defined
| fs : filesystem | lets you access the file system for reading the certificates
*/

const http = require("http");
const https = require("https");
const url = require("url");
const StringDecoder = require("string_decoder").StringDecoder;
const path = require("path");
const fs = require("fs");
const config = require("./config");
const handlers = require("./handlers");
const helpers = require("./helpers");
const _data = require("./data");
const util = require("util");
const debug = util.debuglog("server");

// Instantiate the server
const server = {};

// TODO get rid of this
// helpers.sendTwilioSms(
//   "7702879809",
//   "Did you hear about the monkeys who shared an Amazon account? They were Prime mates",
//   (err) => {
//     if (!err) {
//       debug("Message sent successfully!");
//     } else {
//       debug(err);
//     }
//   }
// );

// helpers.sendTwilioSms(
//     "7702879809",
//     "Did you hear about the monkeys who shared an Amazon account? They were Prime mates",
//     (err) => {
//       debug(`this was the error : ${err}`)
//     }
//   );

// TESTING
// @TODO delete this
// _data.create('test', 'nfile', {'mario' : 'pwincess'}, err => { debug(err); })
// _data.update('test', 'nfile', { 'deno': 'node' }, err => { debug(err); });
// _data.read('test', 'nfile', (err, data) => {
//     debug(`ERROR: `,err,` \nDATA: ${data}`);
// });
// _data.delete('test', 'nfile', (err)=> {
//     debug(err)
// })

/* ------------------ METHODS USED ------------------ 
| http.createServer: takes a callback with two parameters
|                  - request : contains all the resources the user is  asking for
|                  - response
| url.parse: takes 2 parameters
|          - req.url: full url (/home/page/1/)
|          - bool value (true): to parse the query string, 
|                  to set the parsedUrl.query value at the eqivalent to
|                  if we had send this data to the queryString module
|          - returns an OBJECT conataining query, path, pathname etc..
| url.parse.pathname: untrimmed pathname that the user requested
| req.method: gets the HTTP method from the request object [GET/PUT/POST/DELETE/HEAD]
| url.parse(req.url,true).query: selects the query property returned from url.parse()
| req.headers: req is an object having header as a property
*/

// 12.0 create a unified server
//      contains all the server logic for HTTP and HTTPS
server.unifiedServer = (req, res) => {
  //  3.0 get the URL and PARSE it
  const parsedURL = url.parse(req.url, true);

  // 3.1 get the path from the URL
  const path = parsedURL.pathname;

  //  3.2 replace anything that started and ending with / with ''
  // /home/page/ ------ to ------ home/page
  const trimmedPath = path.replace(/^\/+|\/+$/g, "");

  // 4. get the HTTP method from the req
  const method = req.method.toUpperCase();

  // 5. get the queryString as an object with keys and values
  const queryStringObject = parsedURL.query;

  // 6. get the Header as an object with keys and values
  const header = req.headers;

  // 7. get the payload if any
  // 7.1 create a new String Decoder
  //      takes the encoding(UTF-8, UTF-16, etc..)
  //      that needs to be decoded as a parameter
  /*
        | payload come as a part of HTTP request to the server as a STREAM
        | we need to collect that stream (buffer) as it comes in and 
        | when it tells us we are the end (req has an event called 'end') 
        |   req.on('end', param => param)
        | we need combine all that stream into a single string
        | before figuring out what needs to be done with it
        */
  const decoder = new StringDecoder("utf-8");
  let buffer = "";
  /* 
        | as soon as the request emmits the data event, it sends this data to a callback
        | which decodes the data and appends it to the buffer
        | req.on('data', function(data){
        |     buffer += decoder.write(data);
        | })
        */
  req.on("data", (data) => {
    return (buffer += decoder.write(data));
  });
  req.on("end", () => {
    buffer += decoder.end(); // cap-off the buffer with the end

    // 9.2 choose the handelrs the request should go to
    // if a request is not found, select "notFound" handler
    let choosenHandler =
      typeof server.router[trimmedPath] !== "undefined"
        ? server.router[trimmedPath]
        : handlers.notFound;

    //  9.3 construct the data obejct that needs to be sent to the handler
    let data = {
      trimmedPath: trimmedPath,
      queryStringObject: queryStringObject,
      method: method,
      headers: header,
      payload: helpers.parseJsonToObject(buffer),
    };

    // 9.4 route the request to the handler specified in the router
    choosenHandler(data, (statusCode, payLoad) => {
      // use the status code returned by the handler or default it to 200
      statusCode = typeof statusCode === "number" ? statusCode : 200;

      // use the paylaod returned by the handler or default it to an empty object
      payLoad = typeof payLoad === "object" ? payLoad : Object.create(null);

      // convert the payload into string
      let paylaodString = JSON.stringify(payLoad);

      // return the RESPONSE
      res.setHeader("Content-Type", "application/json"); // tells the browser to parse it as JSON
      res.writeHead(statusCode);
      res.end(paylaodString);

      // If the response is 200, print green else print red
      if(statusCode == 200){
        debug("\x1b[32m%s\x1b[0m",`${method.toUpperCase()}/${trimmedPath} ${statusCode}`)
      }else{
        debug("\x1b[31m%s\x1b[0m",`${method.toUpperCase()}/${trimmedPath} ${statusCode}`)
      }
    });

    // // now that the request is ended, we can now send the response and log data
    // // 1.1 send the response --- moved this inside req.on('end', f=>f)
    // res.end("Yo! buddy\n");

    // // 1.2 log the request path --- moved this inside req.on('end', f=>f) --- into choosen Handler
    // debug(`Request Received on: ${trimmedPath} \nmethod: ${method}`);
    // // cant use ${} for objects
    // debug("parsed URL: ", parsedURL);
    // debug("Query String: ", queryStringObject);
    // debug("Headers: ", header);
    // debug("Payload: ", buffer);
  });

  // 1.1 send the response --- moved this inside req.on('end', f=>f)
  // res.end("Yo! buddy\n");

  // 1.2 log the request path --- moved this inside req.on('end', f=>f)
  // debug(`Request Received on: ${trimmedPath} \nmethod: ${method}`);
  // // cant use ${} for objects
  // debug("parsed URL: ", parsedURL);
  // debug("Query String: ", queryStringObject);
  // debug("Headers: ", header);
  // debug()
};

// 1.0 server should respond to all requests with a string
// 12.1 instantiating HTTP server
server.httpServer = http.createServer((req, res) => {
  server.unifiedServer(req, res);
});

// 12.2 create a HttpsServerOptions object
//      reading file synchronously because, the key and cert must be read
//      and verified before proceeding furthur
server.httpsServerOptions = {
  key: fs.readFileSync(path.join(__dirname, "../https/key.pem")),
  cert: fs.readFileSync(path.join(__dirname, "../https/cert.pem")),
  /* 'key': fs.readFile('./https/key.pem',(err)=> {
            debug(err);
        }),
        'cert': fs.readFile('./https/cert.pem', (err)=>{
            debug(err);
        }), */
};

// 12.3 instantiating HTTPS server
server.httpsServer = https.createServer(
  server.httpsServerOptions,
  (req, res) => {
    server.unifiedServer(req, res);
  }
);

// 9. define a request router
server.router = {
  ping: handlers.ping,
  users: handlers.users,
  tokens: handlers.tokens,
  checks: handlers.checks,
};

// Init function
server.init = () => {
  // Start the HTTP server
  // 2. Start the server, and have it listen on port 3030
  server.httpServer.listen(config.httpPort, () => {
    console.log("\x1b[35m%s\x1b[0m",
      `Server Listening on port: ${config.httpPort} in ${config.envName}`
    );
  });

  //   Start the HTTPS server
  // 2. Start the server, and have it listen on port 3030
  server.httpsServer.listen(config.httpsPort, () => {
    console.log("\x1b[34m%s\x1b[0m",
      `Server Listening on port: ${config.httpsPort} in ${config.envName}`
    );
  });
};

// Export the server
module.exports = server;
