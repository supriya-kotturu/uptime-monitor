# Uptime monitor

A RESTful API which lets the user check if the server is up and running
* one user can check only 5 times
* send the user an SMS if there is a change in the server status